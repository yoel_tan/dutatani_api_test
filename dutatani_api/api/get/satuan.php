<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../config/Database.php';
  include_once '../object/Data.php';

  $database = new Database();
  $db = $database->getConnection();

  $sat = new Data($db);
  $result = $sat->satuan();
  $num = $result->rowCount();

  if($num > 0) {
        
    $ins_arr = array();

      while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        array_push($ins_arr, $satuan);
      }

    
      echo json_encode($ins_arr);

} else {
   
  echo 'Satuan not found';
}