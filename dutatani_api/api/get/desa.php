<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../config/Database.php';
  include_once '../object/Data.php';

  $database = new Database();
  $db = $database->getConnection();
  $area = new Data($db);

  $kec = isset($_GET['kec']) ? $_GET['kec'] : die();

  $result = $area->desa($kec);
  $num = $result->rowCount();

  if($num > 0) {
        
    $ins_arr = array();

      while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        array_push($ins_arr, $Nama_Desa);
      }

    
      echo json_encode($ins_arr);

} else {
  echo 'Desa not found';
}