<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../config/Database.php';
  include_once '../object/Data.php';

  $database = new Database();
  $db = $database->getConnection();
  $area = new Data($db);

  $prov = isset($_GET['Nama_Provinsi']) ? $_GET['Nama_Provinsi'] : die();

  $result = $area->kabupaten($prov);
  $num = $result->rowCount();

  if($num > 0) {
        
    $ins_arr = array();

      while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        array_push($ins_arr, $Nama_Kabupaten);
      }

    
      echo json_encode($ins_arr);

} else {   
  echo 'Kabupaten not found';
}