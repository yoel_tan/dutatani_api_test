<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../config/Database.php';
  include_once '../object/Data.php';

  $database = new Database();
  $db = $database->getConnection();
  $area = new Data($db);

  $kab = isset($_GET['Nama_Kabupaten']) ? $_GET['Nama_Kabupaten'] : die();

  $result = $area->kecamatan($kab);
  $num = $result->rowCount();

  if($num > 0) {
        
    $ins_arr = array();

      while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        array_push($ins_arr, $Nama_Kecamatan);
      }

    
      echo json_encode($ins_arr);

} else {
  echo 'Kecamatan not found';
}