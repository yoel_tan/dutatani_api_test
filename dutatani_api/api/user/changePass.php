<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/User.php';

  $database = new Database();
  $db = $database->getConnection();

  $user = new User($db);

  $user->ID_User = isset($_POST['ID_User']) ? $_POST['ID_User'] : die();
  $user->pass = isset($_POST['pass']) ? $_POST['pass'] : die();
  
  if($user->forgotPass()) {
    http_response_code(200);
    echo 'changed';
  } else {
    http_response_code(400);
    echo 'not changed';
  }