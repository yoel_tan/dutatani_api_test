<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/User.php';

  $database = new Database();
  $db = $database->getConnection();

  $user = new User($db);

  $user->ID_User = isset($_POST['ID_User']) ? $_POST['ID_User'] : die();
  
  $result = $user->checkUser();
  $num = $result->rowCount();

  if($num > 0) {
        
      while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        $ins = array(
            'Password' => $Password,
            'jawaban' => $jawaban,
          ); 
      }

      http_response_code(200);
      echo json_encode($ins);

} else {
  http_response_code(404);
  echo 'not found';
}