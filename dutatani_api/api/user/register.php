<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/User.php';


  $database = new Database();
  $db = $database->getConnection();

  $user = new User($db);

  // ambil raw datanya
  $data = json_decode(file_get_contents("php://input"));

//   $kel_tani->ID_Kelompok_Tani = $data->ID_Kelompok_Tani;
//   $kel_tani->Nama_Kelompok_Tani = $data->Nama_Kelompok_Tani;
//   $kel_tani->Alamat_Sekretariat = $data->Alamat_Sekretariat;
//   $kel_tani->Kabupaten = $data->Kabupaten;
//   $kel_tani->Kecamatan = $data->Kecamatan;
//   $kel_tani->Provinsi = $data->Provinsi;
//   $kel_tani->Desa_Kelurahan = $data->Desa_Kelurahan;
//   $kel_tani->Nomor_Telpon = $data->Nomor_Telpon;
//   $kel_tani->Email_Kel_Tani = $data->Email_Kel_Tani;
//   $kel_tani->Kontak_Person = $data->Kontak_Person;
  $user->ID_User = $data->ID_User;
  $user->nama = $data->nama;
  $user->nomor_telpon = $data->nomor_telpon;
  $user->Email = $data->Email;
  $user->Password = $data->Password;
  $user->jawaban = $data->jawaban;
//   $kel_tani->alamat = $data->alamat;
//   $kel_tani->kabupaten = $data->kabupaten;
//   $kel_tani->kecamatan = $data->kecamatan;
//   $kel_tani->provinsi = $data->provinsi;
//   $kel_tani->kelurahan_desa = $data->kelurahan_desa;
//   $kel_tani->Forget_Pass = $data->Forget_Pass;
  
  
  // Create Category
  if($user->regUser()) {
    echo 'User Registration success';
  } else {
    echo 'User Registration failed';
  }