<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../config/Database.php';
  include_once '../object/User.php';

 
  $database = new Database();
  $db = $database->getConnection();


  $user = new User($db);
  $user->ID_User = isset($_POST['ID_User']) ? $_POST['ID_User'] : die();
  $result = $user->read();
  $num = $result->rowCount();

  // 
  if($num > 0) {
        
        $ins_arr = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
          extract($row);

          echo json_encode(
            array(
              'userId' => $user->ID_User,
              'name' => $nama,
              'storeId' => $ID_Kelompok_Tani,
            )
          );
        }

      
        

  } else {
     
    echo 'User not found';
  }