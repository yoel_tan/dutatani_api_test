<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/Laporan.php';
  include_once '../object/Product.php';


  $database = new Database();
  $db = $database->getConnection();

  $laporan = new Laporan($db);
  $produk = new Product($db);

  // ambil raw datanya
  $data = json_decode(file_get_contents("php://input"));

  $laporan->orderId = $data->orderId;
  $laporan->storeId = $data->storeId;
  $laporan->productId = $data->productId;
  $laporan->amount = $data->amount;
  $laporan->orderLocation = $data->orderLocation;
  $laporan->orderDate = $data->orderDate;
  $laporan->kecamatan = $data->kecamatan;
  $laporan->kabupaten = $data->kabupaten;
  $laporan->provinsi = $data->provinsi;  

  // Create Category
  if($laporan->create()) {
    $produk->updateStok($data->amount, $data->productId);
    echo 'Order Created';
  } else {
    echo 'Order not created';
  }