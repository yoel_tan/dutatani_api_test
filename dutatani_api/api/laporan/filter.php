<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/Laporan.php';
  include_once '../object/Product.php';

 
  $database = new Database();
  $db = $database->getConnection();

  $laporan = new Laporan($db);
  $produk = new Product($db);

  $laporan->storeId = isset($_POST['storeId']) ? $_POST['storeId'] : die();

  $result = $laporan->filter();
  $num = $result->rowCount();
  
  if($num > 0) {
        
        $ins_arr = array();
        $ins_prod = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
          extract($row);

          $produk->ID_Produk = $productId;
          $res_prod = $produk->searchID($productId); 

          $ins_prod = array(
            'ID_Produk' => $productId,
            'Nama_Produk' => $produk->Nama_Produk,
            'Satuan' => $produk->Satuan,
          );

          $ins_item = array(
            'orderLocation' => $location,
            'product' => $ins_prod,
            'amount' => $total,
            
          );

        
          array_push($ins_arr, $ins_item);
        }

      
        echo json_encode($ins_arr);

  } else {
     
    echo 'Order not found';
  }