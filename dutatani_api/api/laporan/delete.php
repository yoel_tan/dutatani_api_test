<?php

  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: DELETE');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');
  
  include_once '../config/Database.php';
  include_once '../object/Laporan.php';


  $database = new Database();
  $db = $database->getConnection();

  $laporan = new Laporan($db);

  $laporan->orderId = isset($_POST['orderId']) ? $_POST['orderId'] : die();


  // Delete post
  if($laporan->delete()) {
    echo 'Order deleted';
  } else {
    echo 'Order not deleted';
  }