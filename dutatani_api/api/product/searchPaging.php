<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../config/core.php';
  include_once '../shared/utilities.php';
  include_once '../config/Database.php';
  include_once '../object/Product.php';
  include_once '../object/KelompokTani.php';

  $utilities = new Utilities();
  $database = new Database();
  $db = $database->getConnection();


  $produk = new Product($db);
  $kel_tani = new KelompokTani($db);
  $produk->Nama_Produk = isset($_GET['nama']) ? $_GET['nama'] : die();
  $result = $produk->searchNamaPaging($from_record_num, $records_per_page);
  $num = $result->rowCount();

  // Create array
  if($num > 0) {

        $ins_arr = array();
        $ins_kel_tani = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
          extract($row);

          $kel_tani->ID_Kelompok_Tani = $ID_Kelompok_Tani;
          $res_kel_tani = $kel_tani->searchID($ID_Kelompok_Tani); 

          $ins_kel_tani = array(
            'ID_Kelompok_Tani' => $kel_tani->ID_Kelompok_Tani,
            'Nama_Kelompok_Tani' => $kel_tani->Nama_Kelompok_Tani,
            'Kontak_Person' => $kel_tani->Kontak_Person,
            'Nomor_Telpon' => $kel_tani->Nomor_Telpon,
            'Foto1' => $kel_tani->Foto1,
            'Alamat_Sekretariat' => $kel_tani->Alamat_Sekretariat,
            'Kecamatan' => $kel_tani->Kecamatan,
            'Kabupaten' => $kel_tani->Kabupaten,
            'Provinsi' => $kel_tani->Provinsi,
            'nama_admin' => $kel_tani->nama_admin,
            'Email' => $kel_tani->Email,
            'Tgl_Terbentuk' => $kel_tani->Tgl_Terbentuk,
          );

          

          $ins_item = array(
            'ID_Produk' => $ID_Produk,
            'Nama_Produk' => $Nama_Produk,
            'Deskripsi_Produk' => $Deskripsi_Produk,
            'Satuan' => $Satuan,
            'Stok' => $Stok,
            'Status_Produk' => $Status_Produk,
            'Harga' => $Harga,
            'Gambar_Produk' => $Gambar_Produk,
            'Kelompok_Tani' => $ins_kel_tani,
            'Tgl_Panen' => $Tgl_Panen,
            'Kategori' => $Kategori,
          );

          http_response_code(200);
          array_push($ins_arr, $ins_item);
        }

      
        echo json_encode($ins_arr);

  } else {
    http_response_code(404);
    echo 'Product not found';
  }