<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/Product.php';

  $database = new Database();
  $db = $database->getConnection();

  $produk = new Product($db);

  // ambil raw datanya
  $data = json_decode(file_get_contents("php://input"));

  $produk->ID_Produk = $data->ID_Produk;
  $produk->Nama_Produk = $data->Nama_Produk;
  $produk->Deskripsi_Produk = $data->Deskripsi_Produk;
  $produk->Satuan = $data->Satuan;
  $produk->Stok = $data->Stok;
  $produk->Status_Produk = $data->Status_Produk;
  $produk->Harga = $data->Harga;
  $produk->Gambar_Produk = $data->Gambar_Produk;
  $produk->ID_Kelompok_Tani = $data->ID_Kelompok_Tani;  
  $produk->Tgl_Panen = $data->Tgl_Panen;
  $produk->Kategori = $data->Kategori;

  // Create Category
  if($produk->create()) {
    echo 'Product Created';
  } else {
    echo 'Product not created';
  }