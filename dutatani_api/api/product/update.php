<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: PATCH');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/Product.php';


  $database = new Database();
  $db = $database->getConnection();

  $produk = new Product($db);

  // ambil raw datanya
  $data = json_decode(file_get_contents("php://input"));

  // Set ID to UPDATE
  $produk->ID_Produk = $data->ID_Produk;
  $produk->Nama_Produk = $data->Nama_Produk;
  $produk->Deskripsi_Produk = $data->Deskripsi_Produk;
  $produk->Satuan = $data->Satuan;
  $produk->Stok = $data->Stok;
  $produk->Status_Produk = $data->Status_Produk;
  $produk->Harga = $data->Harga;
  $produk->Gambar_Produk = $data->Gambar_Produk;
  $produk->Tgl_Panen = $data->Tgl_Panen;
  $produk->Kategori = $data->Kategori;

  // echo json_encode(
  //   array(
  //       "Data ID_Produk" => $data->ID_Produk,
  //       "User ID_Produk" => $produk->ID_Produk,
  //       "Data Nama_Produk" => $data->Nama_Produk,
  //       "User Nama_Produk" => $produk->Nama_Produk,
  //       "Data Deskripsi_Produk" => $data->Deskripsi_Produk,
  //       "User Deskripsi_Produk" => $produk->Deskripsi_Produk,
  //       "Data Stok" => $data->Stok,
  //       "User Stok" => $produk->Stok,
  //       "Data Status_Produk" => $data->Status_Produk,
  //       "User Status_Produk" => $produk->Status_Produk,
  //       "Data Gambar_Produk" => $data->Status_Produk,
  //       "User Status_Produk" => $produk->Status_Produk
  //   )
  //   );

  // Update 
  if($produk->update()) {
    echo 'Product updated';
  } else {
    echo 'Product not updated';
  }