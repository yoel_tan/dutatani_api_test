<?php

  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');
  
  include_once '../config/Database.php';
  include_once '../object/Product.php';


  $database = new Database();
  $db = $database->getConnection();

  $produk = new Product($db);

  // Get raw posted data
  $produk->ID_Produk = isset($_POST['ID_Produk']) ? $_POST['ID_Produk'] : die();

  // echo json_encode(
  //   array(
  //       "Data ID_Produk" => $data->ID_Produk,
  //       "User ID_Produk" => $produk->ID_Produk
  //   )
  //   );

  // Delete post
  if($produk->delete()) {
    echo 'Product deleted';
  } else {
    echo 'Product not deleted';
  }