<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/Product.php';
  include_once '../object/KelompokTani.php';

 
  $database = new Database();
  $db = $database->getConnection();
  $produk = new Product($db);

  $produk->ID_Kelompok_Tani = isset($_POST['ID_Kelompok_Tani']) ? $_POST['ID_Kelompok_Tani'] : die();
  $produk->Nama_Produk = isset($_POST['Nama_Produk']) ? $_POST['Nama_Produk'] : die();

  $result = $produk->searchProdukAdmin();
  $num = $result->rowCount();

  if($num > 0) {
        
      $ins_arr = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
          extract($row);

          $ins_item = array(
            'ID_Produk' => $ID_Produk,
            'Nama_Produk' => $Nama_Produk,
            'Deskripsi_Produk' => $Deskripsi_Produk,
            'Satuan' => $Satuan,
            'Stok' => $Stok,
            'Status_Produk' => $Status_Produk,
            'Harga' => $Harga,
            'Gambar_Produk' => $Gambar_Produk,
            'Tgl_Panen' => $Tgl_Panen,
            'Kategori' => $Kategori,
          );
        
          array_push($ins_arr, $ins_item);
        }

      
        echo json_encode($ins_arr);

  } else {
     
    echo 'Product not found';
  }