<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: PATCH');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/KelompokTani.php';


  $database = new Database();
  $db = $database->getConnection();

  $kel_tani = new KelompokTani($db);

  // ambil raw datanya
  $data = json_decode(file_get_contents("php://input"));

  
  // Set ID to UPDATE
  $kel_tani->ID_Kelompok_Tani = $data->ID_Kelompok_Tani;
  $kel_tani->Nama_Kelompok_Tani = $data->Nama_Kelompok_Tani;
  $kel_tani->Email = $data->Email;
  $kel_tani->Kontak_Person = $data->Kontak_Person;
  $kel_tani->Foto1 = $data->Foto1;

  echo json_encode(
    array(
        "Data ID_Kelompok_Tani" => $data->ID_Kelompok_Tani,
        "User ID_Kelompok_Tani" => $kel_tani->ID_Kelompok_Tani,
        "Data Nama_Kelompok_Tani" => $data->Nama_Kelompok_Tani,
        "User Nama_Kelompok_Tani" => $kel_tani->Nama_Kelompok_Tani,
        "Data Email" => $data->Email,
        "User Email" => $kel_tani->Email,
        "Data Kontak_Person" => $data->Kontak_Person,
        "User Kontak_Person" => $kel_tani->Kontak_Person,
        "Data Foto1" => $data->Foto1,
        "User Foto1" => $kel_tani->Foto1
    )
    );

  // Update 
  if($kel_tani->update()) {
    echo 'Kelompok Tani updated';
  } else {
    echo 'Kelompok Tani not updated';
  }