<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: PATCH');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/KelompokTani.php';
  include_once '../object/User.php';

  $database = new Database();
  $db = $database->getConnection();

  $kel_tani = new KelompokTani($db);
  $user = new User($db);

  // ambil raw datanya
  $data = json_decode(file_get_contents("php://input"));
  
  // Set ID to UPDATE
  $kel_tani->ID_Kelompok_Tani = $data->ID_Kelompok_Tani;
  $kel_tani->Nama_Kelompok_Tani = $data->Nama_Kelompok_Tani;
  $kel_tani->Alamat_Sekretariat = $data->Alamat_Sekretariat;
  $kel_tani->Nomor_Telpon = $data->nomor_telpon;
  $kel_tani->Email = $data->Email;
  $user->ID_User = $data->ID_User;
  $user->nama = $data->nama;
  $user->nomor_telpon = $data->nomor_telpon;
  $user->Email = $data->Email;
  // $kel_tani->Email_Kel_Tani = $data->Email_Kel_Tani;
  // $kel_tani->Kontak_Person = $data->Kontak_Person;
  // $kel_tani->Nomor_Telpon = $data->Nomor_Telpon;

  // echo json_encode(
  //   array(
  //       "Data ID_Kelompok_Tani" => $data->ID_Kelompok_Tani,
  //       "User ID_Kelompok_Tani" => $kel_tani->ID_Kelompok_Tani,
  //       "Data Nama_Kelompok_Tani" => $data->Nama_Kelompok_Tani,
  //       "User Nama_Kelompok_Tani" => $kel_tani->Nama_Kelompok_Tani,
  //       "Data Alamat_Sekretariat" => $data->Alamat_Sekretariat,
  //       "User Alamat_Sekretariat" => $kel_tani->Alamat_Sekretariat,
  //       // "Data Email_Kel_Tani" => $data->Email_Kel_Tani,
  //       // "User Email_Kel_Tani" => $kel_tani->Email_Kel_Tani,
  //       // "Data Kontak_Person" => $data->Kontak_Person,
  //       // "User Kontak_Person" => $kel_tani->Kontak_Person,
  //       // "Data Foto1" => $data->Nomor_Telpon,
  //       // "User Foto1" => $kel_tani->Nomor_Telpon
  //   )
  //   );

  // Update 
  if($kel_tani->updateStore()) {
    if ($user->updateUser()) {
      echo 'Update success';
    }
  } else {
    echo 'Update failed';
  }