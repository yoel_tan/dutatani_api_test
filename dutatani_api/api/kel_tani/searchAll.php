<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../config/Database.php';
  include_once '../object/KelompokTani.php';


  $database = new Database();
  $db = $database->getConnection();


  $kel_tani = new KelompokTani($db);
  $kel_tani->Nama_Kelompok_Tani = isset($_GET['nama']) ? $_GET['nama'] : die();
  $result = $kel_tani->searchAll();
  $num = $result->rowCount();
  
  // Create array
  if($num > 0) {
        
        $ins_arr = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
          extract($row);

          $ins_item = array(
            'ID_Kelompok_Tani' => $ID_Kelompok_Tani,
            'Nama_Kelompok_Tani' => $Nama_Kelompok_Tani,
            'Kontak_Person' => $Kontak_Person,
            'Nomor_Telpon' => $Nomor_Telpon,
            'Foto1' => $Foto1,
            'Alamat_Sekretariat' => $Alamat_Sekretariat,
            'Kecamatan' => $Kecamatan,
            'Kabupaten' => $Kabupaten,
            'Provinsi' => $Provinsi,
            'ID_User' => $ID_User,
            'nama_admin' => $nama_admin,
            'Tgl_Terbentuk' => $Tgl_Terbentuk,
          );

        
          array_push($ins_arr, $ins_item);
        }

      
        echo json_encode($ins_arr);

  } else {
     
        echo 'No store found';
  }