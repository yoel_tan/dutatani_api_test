<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/KelompokTani.php';
  include_once '../object/Product.php';
 
  $database = new Database();
  $db = $database->getConnection();

  $produk = new Product($db);
  $kelompok_tani = new KelompokTani($db);

  $kelompok_tani->ID_User = isset($_POST['ID_User']) ? $_POST['ID_User'] : die();
  
  $result = $kelompok_tani->searchIdUser($kelompok_tani->ID_User);
  $produk->ID_Kelompok_Tani = $kelompok_tani->ID_Kelompok_Tani;
  $res_produk = $produk->readOnKelompokTani();
  $num = $res_produk->rowCount();

  $ins_arr = array();
  $ins_prod = array();

  if($num > 0) {        

        while($row = $res_produk->fetch(PDO::FETCH_ASSOC)) {
          extract($row);

          $ins_prod = array(
            'ID_Produk' => $ID_Produk,
            'Nama_Produk' => $Nama_Produk,
            'Deskripsi_Produk' => $Deskripsi_Produk,
            'Satuan' => $Satuan,
            'Stok' => $Stok,
            'Status_Produk' => $Status_Produk,
            'Harga' => $Harga,
            'Gambar_Produk' => $Gambar_Produk,
            'Tgl_Panen' => $Tgl_Panen,
            'Kategori' => $Kategori,
          );
        
          array_push($ins_arr, $ins_prod);
        }

  }

  $ins_item = array(
    'ID_Kelompok_Tani' => $kelompok_tani->ID_Kelompok_Tani,
    'Nama_Kelompok_Tani' => $kelompok_tani->Nama_Kelompok_Tani,
    'Kontak_Person' => $kelompok_tani->Kontak_Person,
    'Nomor_Telpon' => $kelompok_tani->Nomor_Telpon,
    'Foto1' => $kelompok_tani->Foto1,
    'Alamat_Sekretariat' => $kelompok_tani->Alamat_Sekretariat,
    'Kecamatan' => $kelompok_tani->Kecamatan,
    'Kabupaten' => $kelompok_tani->Kabupaten,
    'Provinsi' => $kelompok_tani->Provinsi,
    'ID_User' => $kelompok_tani->ID_User,
    'nama_admin' => $kelompok_tani->nama_admin,
    'Email' => $kelompok_tani->Email,
    'Tgl_Terbentuk' => $kelompok_tani->Tgl_Terbentuk,
    'productList' => $ins_arr,
  );

  // array_push($ins_item);
  echo json_encode($ins_item);