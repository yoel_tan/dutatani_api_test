<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/KelompokTani.php';


  $database = new Database();
  $db = $database->getConnection();

  $kel_tani = new KelompokTani($db);

  // ambil raw datanya
  $data = json_decode(file_get_contents("php://input"));

  $kel_tani->ID_Kelompok_Tani = $data->ID_Kelompok_Tani;
  $kel_tani->Nama_Kelompok_Tani = $data->Nama_Kelompok_Tani;
  $kel_tani->Alamat_Sekretariat = $data->Alamat_Sekretariat;
  $kel_tani->Kabupaten = $data->Kabupaten;
  $kel_tani->Kecamatan = $data->Kecamatan;
  $kel_tani->Provinsi = $data->Provinsi;
  $kel_tani->Desa_Kelurahan = $data->Desa_Kelurahan;
  $kel_tani->ID_User = $data->ID_User;
  $kel_tani->Nomor_Telpon = $data->Nomor_Telpon;
  $kel_tani->Email = $data->Email;
  // $kel_tani->Kontak_Person = $data->Kontak_Person;
  
  // $kel_tani->nama = $data->nama;
  // $kel_tani->nomor_telpon = $data->nomor_telpon;
  // $kel_tani->Email = $data->Email;
  // $kel_tani->alamat = $data->alamat;
  // $kel_tani->kabupaten = $data->kabupaten;
  // $kel_tani->kecamatan = $data->kecamatan;
  // $kel_tani->provinsi = $data->provinsi;
  // $kel_tani->kelurahan_desa = $data->kelurahan_desa;
  // $kel_tani->Forget_Pass = $data->Forget_Pass;
  // $kel_tani->Password = $data->Password;
  
  // Create Category
  if($kel_tani->regis()) {
    echo 'Store Registration success';
  } else {
    echo 'Store Registration failed';
  }