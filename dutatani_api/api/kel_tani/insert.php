<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/KelompokTani.php';


  $database = new Database();
  $db = $database->getConnection();

  $kel_tani = new KelompokTani($db);

  // ambil raw datanya
  $data = json_decode(file_get_contents("php://input"));

  $kel_tani->ID_Kelompok_Tani = $data->ID_Kelompok_Tani;
  $kel_tani->Nama_Kelompok_Tani = $data->Nama_Kelompok_Tani;
  $kel_tani->Email = $data->Email;
  $kel_tani->Kontak_Person = $data->Kontak_Person;
  $kel_tani->Foto1 = $data->Foto1;
  

  // Create Category
  if($kel_tani->create()) {
    echo 'Kelompok Tani created';
  } else {
    echo 'Kelompok Tani not created';
  }