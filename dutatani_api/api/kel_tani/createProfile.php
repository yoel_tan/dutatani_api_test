<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/KelompokTani.php';


  $database = new Database();
  $db = $database->getConnection();

  $kel_tani = new KelompokTani($db);

  // ambil raw datanya
  $data = json_decode(file_get_contents("php://input"));

  $kel_tani->ID_Kelompok_Tani = $data->ID_Kelompok_Tani;
  $kel_tani->Nama_Kelompok_Tani = $data->Nama_Kelompok_Tani;
  $kel_tani->Alamat_Sekretariat = $data->Alamat_Sekretariat;
  $kel_tani->Email_Kel_Tani = $data->Email_Kel_Tani;
  $kel_tani->Kontak_Person = $data->Kontak_Person;
  $kel_tani->Nomor_Telpon = $data->Nomor_Telpon;
  $kel_tani->ID_User = $data->ID_User;
  

  // Create Category
  if($kel_tani->createProfile()) {
    echo json_encode(
      array('message' => 'Admin Created')
    );
  } else {
    echo json_encode(
      array('message' => 'Admin Not Created')
    );
  }