<?php

  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: DELETE');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');
  
  include_once '../config/Database.php';
  include_once '../object/KelompokTani.php';


  $database = new Database();
  $db = $database->getConnection();

  $kel_tani = new KelompokTani($db);


  // Get raw posted data
  $data = json_decode(file_get_contents("php://input"));

  // Set ID to DELETE
  $kel_tani->ID_Kelompok_Tani = $data->ID_Kelompok_Tani;

  echo json_encode(
    array(
        "Data ID_Kelompok_Tani" => $data->ID_Kelompok_Tani,
        "User ID_Kelompok_Tani" => $kel_tani->ID_Kelompok_Tani
    )
    );


  // Delete post
  if($kel_tani->delete()) {
    echo 'Kelompok Tani deleted';
  } else {
    echo 'Kelompok Tani not deleted';
  }