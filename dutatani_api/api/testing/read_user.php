<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../config/Database.php';
  include_once '../object/Tester.php';

  $database = new Database();
  $db = $database->getConnection();
  $tester = new Tester($db);

  $tester->userId = isset($_GET['id']) ? $_GET['id'] : die();

  $result = $tester->read();
  $num = $result->rowCount();

  if($num > 0) {
        
    while($row = $result->fetch(PDO::FETCH_ASSOC)) {
      extract($row);

      $ins_item = array(
        'id' => $tester->userId,
        'name' => $name,
        'contact' => $contact,
        'birthYear' => $birthYear,
        'createdDate' => $createdDate,
      );
    }

    http_response_code(200);
    echo json_encode($ins_item);

} else {
  http_response_code(404);
  echo 'Not found';
}