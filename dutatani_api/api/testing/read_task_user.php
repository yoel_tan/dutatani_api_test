<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../config/Database.php';
  include_once '../object/Tester.php';
  include_once '../object/TestActivity.php';

  $database = new Database();
  $db = $database->getConnection();
  $act = new TestActivity($db);
  $tester = new Tester($db);

  $act->user = isset($_GET['user']) ? $_GET['user'] : die();

  $result = $act->readTaskUser();
  $num = $result->rowCount();

  if($num > 0) {
    http_response_code(200);
    echo json_encode($result->fetchAll(PDO::FETCH_GROUP|\PDO::FETCH_ASSOC));  

} else {
  http_response_code(404);
  echo 'Not found';
}