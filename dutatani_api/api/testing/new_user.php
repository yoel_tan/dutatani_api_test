<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/Tester.php';


  $database = new Database();
  $db = $database->getConnection();

  $tester = new Tester($db);

  // ambil raw datanya
  $data = json_decode(file_get_contents("php://input"));

  $tester->userId = $data->userId;
  $tester->name = $data->name;
  $tester->contact = $data->contact;
  $tester->birthYear = $data->birthYear;
  $tester->createdDate = $data->createdDate;
  
  if($tester->create()) {
    echo 'tester created';
  } else {
    echo 'tester not created';
  }