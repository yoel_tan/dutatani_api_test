<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../config/Database.php';
  include_once '../object/Tester.php';

  $database = new Database();
  $db = $database->getConnection();
  $tester = new Tester($db);

  $tester->name = isset($_GET['name']) ? $_GET['name'] : die();

  $result = $tester->read();
  $num = $result->rowCount();

  if($num > 0) {
    $result->fetch(PDO::FETCH_ASSOC);
    echo 'valid';
  } else {
    echo 'invalid';
  }