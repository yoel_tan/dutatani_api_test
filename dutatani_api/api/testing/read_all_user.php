<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../config/Database.php';
  include_once '../object/Tester.php';

  $database = new Database();
  $db = $database->getConnection();
  $tester = new Tester($db);

  $result = $tester->readAll();
  $num = $result->rowCount();

  if($num > 0) {
        
    $ins_arr = array();

    while($row = $result->fetch(PDO::FETCH_ASSOC)) {
      extract($row);

      $ins_item = array(
        'id' => $userId,
        'name' => $name,
        'contact' => $contact,
        'birthYear' => $birthYear,
        'createdDate' => $createdDate,
      );

    
      array_push($ins_arr, $ins_item);
    }

    http_response_code(200);
    echo json_encode($ins_arr);

} else {
  http_response_code(404);
  echo 'Not found';
}