<?php
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

  include_once '../config/Database.php';
  include_once '../object/TestActivity.php';


  $database = new Database();
  $db = $database->getConnection();

  $act = new TestActivity($db);

  // ambil raw datanya
  $data = json_decode(file_get_contents("php://input"));

  $act->session = $data->session;
  $act->user = $data->user;
  $act->activities = $data->activities;
  $act->activityTime = $data->activityTime;
  
  if($act->create()) {
    echo 'activity created';
  } else {
    echo 'activity not created';
  }