<?php
class Laporan{
  
    // tempat setting koneksi dan nama tabel 
    private $conn;
    private $table = "trans_pemesanan_ecommerce";
  
    //field table nya dimasukin  
    public $orderId;
    public $storeId;
    public $productId;
    public $amount;
    public $orderLocation;
    public $orderDate;
    public $kecamatan;
    public $kabupaten;
    public $provinsi;
  
    // bikin construct untuk konesi database
    public function __construct($db){
        $this->conn = $db;
    
    }
    

    // Get categories
    public function read() {
        // query
        $query = "SELECT * from trans_pemesanan_ecommerce WHERE storeId = ?";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->storeId);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }

      public function filter() {
        $query = "SELECT CONCAT(t.kabupaten, ', ', t.provinsi) AS location, t.productId, SUM(t.amount) AS total FROM trans_pemesanan_ecommerce AS t, master_produk_tani AS p WHERE t.productId = p.ID_Produk AND t.storeId = ? GROUP BY kabupaten, p.ID_Produk";
      
        $stmt =$this->conn->prepare($query);
        $stmt->bindParam(1, $this->storeId);

        $stmt->execute();

        return $stmt;
      }

      public function create() {
        // Create Query
        $query = "INSERT into trans_pemesanan_ecommerce VALUES (:orderId, :storeId
        , :productId, :amount, :orderLocation, :orderDate
        , :kecamatan, :kabupaten, :provinsi)";
    
      // Prepare Statement
      $stmt = $this->conn->prepare($query);
  
      $this->orderId = htmlspecialchars(strip_tags($this->orderId));
      $this->storeId = htmlspecialchars(strip_tags($this->storeId));
      $this->productId = htmlspecialchars(strip_tags($this->productId));
      $this->amount = htmlspecialchars(strip_tags($this->amount));
      $this->orderLocation = htmlspecialchars(strip_tags($this->orderLocation));
      $this->orderDate = htmlspecialchars(strip_tags($this->orderDate));
      $this->kecamatan = htmlspecialchars(strip_tags($this->kecamatan));
      $this->kabupaten = htmlspecialchars(strip_tags($this->kabupaten));
      $this->provinsi = htmlspecialchars(strip_tags($this->provinsi));
  
      // Bind data
      $stmt-> bindParam(':orderId', $this->orderId);
      $stmt-> bindParam(':storeId', $this->storeId);
      $stmt-> bindParam(':productId', $this->productId);
      $stmt-> bindParam(':amount', $this->amount);
      $stmt-> bindParam(':orderLocation', $this->orderLocation);
      $stmt-> bindParam(':orderDate', $this->orderDate);
      $stmt-> bindParam(':kecamatan', $this->kecamatan);
      $stmt-> bindParam(':kabupaten', $this->kabupaten);
      $stmt-> bindParam(':provinsi', $this->provinsi);
    
      // Execute query
      if($stmt->execute()) {
        return true;
      }
      printf("Error: $s.\n", $stmt->error);
      return false;
      }

      public function delete() {
    
        $query = "DELETE FROM trans_pemesanan_ecommerce WHERE orderId = :orderId";
        $stmt = $this->conn->prepare($query);
    
        // clean data
        $this->orderId = htmlspecialchars(strip_tags($this->orderId));
    
        // Bind Data
        $stmt-> bindParam(':orderId', $this->orderId);
    
        // Execute query
        if($stmt->execute()) {
          return true;
        }
        printf("Error: $s.\n", $stmt->error);
    
        return false;
        }

    }

    