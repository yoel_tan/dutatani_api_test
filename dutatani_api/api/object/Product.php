<?php
class Product{
  
    // tempat setting koneksi dan nama tabel 
    private $conn;
    private $table = "master_produk_tani";
  
    //field table nya dimasukin  
    public $ID_Produk;
    public $Nama_Produk;
    public $Deskripsi_Produk;
    public $Satuan;
    public $Stok;
    public $Status_Produk;
    public $Harga;
    public $Gambar_Produk;
    public $ID_Kelompok_Tani;
    public $Tgl_Panen;
    public $Kategori;
  
    // bikin construct untuk konesi database
    public function __construct($db){
        $this->conn = $db;
    
    }
    

    // Get categories
    public function read() {
      // query
      $query = 'select * from ' .$this->table.'';
      // Prepare statement
      $stmt = $this->conn->prepare($query);
      // Execute 
      $stmt->execute();

      return $stmt;
    }

    public function readPaging($from_record_num, $records_per_page){
  
      // select query
      $query = "SELECT * from master_produk_tani LIMIT ?, ?";
    
      // prepare query statement
      $stmt = $this->conn->prepare( $query );
    
      // bind variable values
      $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
      $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
    
      // execute query
      $stmt->execute();
    
      // return values from database
      return $stmt;
    }
    
    // used for paging products
    public function count(){
      $query = "SELECT COUNT(*) as total_rows FROM " . $this->table . "";
    
      $stmt = $this->conn->prepare( $query );
      $stmt->execute();
      $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
      return $row['total_rows'];
    }

    public function readOnKelompokTani() {
      // query
      $query = "SELECT * from master_produk_tani WHERE ID_Kelompok_Tani = ? ";
      // Prepare statement
      $stmt = $this->conn->prepare($query);
      $stmt->bindParam(1, $this->ID_Kelompok_Tani);
      // Execute 
      $stmt->execute();

      return $stmt;
    }
  
      // cari spesifik nama produk
    public function searchNamaPaging($from_record_num, $records_per_page){
      // Create query
      $query = "SELECT * from master_produk_tani WHERE Nama_Produk LIKE ? LIMIT ?, ?";
        //Prepare statement
        $stmt = $this->conn->prepare($query);
        $this->Nama_Produk = "%".$this->Nama_Produk."%";
        // Bind ID
        $stmt->bindParam(1, $this->Nama_Produk);
        $stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);
  
        // Execute query
        $stmt->execute();
  
        return $stmt;
    }

    public function searchNama(){
      // Create query
      $query = "SELECT * from master_produk_tani WHERE Nama_Produk LIKE ? ";
        //Prepare statement
        $stmt = $this->conn->prepare($query);
        $this->Nama_Produk = "%".$this->Nama_Produk."%";
        // Bind ID
        $stmt->bindParam(1, $this->Nama_Produk);
  
        // Execute query
        $stmt->execute();
  
        return $stmt;
    }

    public function searchKategori($from_record_num, $records_per_page){
      // Create query
      $query = "SELECT * from master_produk_tani WHERE Kategori = ? LIMIT ?, ?";
        //Prepare statement
        $stmt = $this->conn->prepare($query);
        // Bind ID
        $stmt->bindParam(1, $this->Kategori);
        $stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);
  
        // Execute query
        $stmt->execute();
  
        return $stmt;
    }

    public function searchProdukAdmin(){
      // Create query
      $query = "SELECT * from master_produk_tani WHERE ID_Kelompok_Tani = ? AND Nama_Produk LIKE ? ";
        //Prepare statement
        $stmt = $this->conn->prepare($query);
        $this->Nama_Produk = "%".$this->Nama_Produk."%";
        // Bind ID
        $stmt->bindParam(1, $this->ID_Kelompok_Tani);
        $stmt->bindParam(2, $this->Nama_Produk);
  
        // Execute query
        $stmt->execute();
  
        return $stmt;
    }

    public function searchID($id){
      // Create query
      $query = "SELECT Nama_Produk, Satuan FROM master_produk_tani where ID_Produk = '$id' ";
        $id = htmlspecialchars(strip_tags($id));
        //Prepare statement
        $stmt = $this->conn->prepare($query);
    
        // Execute query
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
  
        // set properties
        $this->Nama_Produk = $row['Nama_Produk'];
        $this->Satuan = $row['Satuan'];
    }
  
  
    public function create() {
      // Create Query
      $query = 'insert into '.$this->table.' VALUES (:ID_Produk, :Nama_Produk
      , :Deskripsi_Produk, :Satuan, :Stok, :Status_Produk
      , :Harga, :Gambar_Produk, :ID_Kelompok_Tani, :Tgl_Panen, :Kategori)';
  
    // Prepare Statement
    $stmt = $this->conn->prepare($query);

    $this->ID_Produk = htmlspecialchars(strip_tags($this->ID_Produk));
    $this->Nama_Produk = htmlspecialchars(strip_tags($this->Nama_Produk));
    $this->Deskripsi_Produk = htmlspecialchars(strip_tags($this->Deskripsi_Produk));
    $this->Satuan = htmlspecialchars(strip_tags($this->Satuan));
    $this->Stok = htmlspecialchars(strip_tags($this->Stok));
    $this->Status_Produk = htmlspecialchars(strip_tags($this->Status_Produk));
    $this->Harga = htmlspecialchars(strip_tags($this->Harga));
    $this->Gambar_Produk = htmlspecialchars(strip_tags($this->Gambar_Produk));
    $this->ID_Kelompok_Tani = htmlspecialchars(strip_tags($this->ID_Kelompok_Tani));
    $this->Tgl_Panen = htmlspecialchars(strip_tags($this->Tgl_Panen));
    $this->Kategori = htmlspecialchars(strip_tags($this->Kategori));

    // Bind data
    $stmt-> bindParam(':ID_Produk', $this->ID_Produk);
    $stmt-> bindParam(':Nama_Produk', $this->Nama_Produk);
    $stmt-> bindParam(':Deskripsi_Produk', $this->Deskripsi_Produk);
    $stmt-> bindParam(':Satuan', $this->Satuan);
    $stmt-> bindParam(':Stok', $this->Stok);
    $stmt-> bindParam(':Status_Produk', $this->Status_Produk);
    $stmt-> bindParam(':Harga', $this->Harga);
    $stmt-> bindParam(':Gambar_Produk', $this->Gambar_Produk);
    $stmt-> bindParam(':ID_Kelompok_Tani', $this->ID_Kelompok_Tani);
    $stmt-> bindParam(':Tgl_Panen', $this->Tgl_Panen);
    $stmt-> bindParam(':Kategori', $this->Kategori);
  
    // Execute query
    if($stmt->execute()) {
      return true;
    }
    printf("Error: $s.\n", $stmt->error);
    return false;
    }
  





    // Update Category
    public function update() {
      // Create Query
      $query = "UPDATE master_produk_tani
      SET Nama_Produk = CASE WHEN COALESCE(:Nama_Produk, '') = '' THEN Nama_Produk ELSE :Nama_Produk END,
      Deskripsi_Produk = CASE WHEN COALESCE(:Deskripsi_Produk, '') = '' THEN Deskripsi_Produk ELSE :Deskripsi_Produk END,
      Satuan = CASE WHEN COALESCE(:Satuan, '') = '' THEN Satuan ELSE :Satuan END,
      Stok = CASE WHEN COALESCE(:Stok, '') = '' THEN Stok ELSE :Stok END,
      Status_Produk = CASE WHEN COALESCE(:Status_Produk, '') = '' THEN Status_Produk ELSE :Status_Produk END,
      Harga = CASE WHEN COALESCE(:Harga, '') = '' THEN Harga ELSE :Harga END,
      Gambar_Produk = CASE WHEN COALESCE(:Gambar_Produk, '') = '' THEN Gambar_Produk ELSE :Gambar_Produk END,
      Tgl_Panen = CASE WHEN COALESCE(:Tgl_Panen, '') = '' THEN Tgl_Panen ELSE :Tgl_Panen END,
      Kategori = CASE WHEN COALESCE(:Kategori, '') = '' THEN Kategori ELSE :Kategori END
      WHERE ID_Produk = :ID_Produk";
  
    // Prepare Statement
    $stmt = $this->conn->prepare($query);
  
    // Clean data
    $this->ID_Produk = htmlspecialchars(strip_tags($this->ID_Produk));
    $this->Nama_Produk = htmlspecialchars(strip_tags($this->Nama_Produk));
    $this->Deskripsi_Produk = htmlspecialchars(strip_tags($this->Deskripsi_Produk));
    $this->Satuan = htmlspecialchars(strip_tags($this->Satuan));
    $this->Stok = htmlspecialchars(strip_tags($this->Stok));
    $this->Status_Produk = htmlspecialchars(strip_tags($this->Status_Produk));
    $this->Harga = htmlspecialchars(strip_tags($this->Harga));
    $this->Gambar_Produk = htmlspecialchars(strip_tags($this->Gambar_Produk));
    $this->ID_Kelompok_Tani = htmlspecialchars(strip_tags($this->ID_Kelompok_Tani));
    $this->Tgl_Panen = htmlspecialchars(strip_tags($this->Tgl_Panen));
    $this->Kategori = htmlspecialchars(strip_tags($this->Kategori));

    // Bind data
    $stmt-> bindParam(':ID_Produk', $this->ID_Produk);
    $stmt-> bindParam(':Nama_Produk', $this->Nama_Produk);
    $stmt-> bindParam(':Deskripsi_Produk', $this->Deskripsi_Produk);
    $stmt-> bindParam(':Satuan', $this->Satuan);
    $stmt-> bindParam(':Stok', $this->Stok);
    $stmt-> bindParam(':Status_Produk', $this->Status_Produk);
    $stmt-> bindParam(':Harga', $this->Harga);
    $stmt-> bindParam(':Gambar_Produk', $this->Gambar_Produk);
    $stmt-> bindParam(':ID_Kelompok_Tani', $this->ID_Kelompok_Tani);
    $stmt-> bindParam(':Tgl_Panen', $this->Tgl_Panen);
    $stmt-> bindParam(':Kategori', $this->Kategori);
  
    // Execute query
    if($stmt->execute()) {
      return true;
    }
  
    // Print error if something goes wrong
    printf("Error: $s.\n", $stmt->error);
  
    return false;
    }

    public function updateStok($jml, $id) {
      $query = "UPDATE master_produk_tani SET Stok = Stok - '$jml', Status_Produk = IF(Stok=0, 'KOSONG', Status_Produk) WHERE ID_Produk = '$id'";
      $stmt = $this->conn->prepare($query);

      $jml = htmlspecialchars(strip_tags($jml));
      $id = htmlspecialchars(strip_tags($id));

      if($stmt->execute()) {
        return true;
      }
      printf("Error: $s.\n", $stmt->error);
  
      return false;

    }
  
    // Delete 
    public function delete() {
    
      $query = "DELETE FROM master_produk_tani WHERE ID_Produk = :ID_Produk";
      $stmt = $this->conn->prepare($query);
  
      // clean data
      $this->ID_Produk = htmlspecialchars(strip_tags($this->ID_Produk));
  
      // Bind Data
      $stmt-> bindParam(':ID_Produk', $this->ID_Produk);
  
      // Execute query
      if($stmt->execute()) {
        return true;
      }
      printf("Error: $s.\n", $stmt->error);
  
      return false;
      }
    }