<?php
class Tester{
  
    // tempat setting koneksi dan nama tabel 
    private $conn;
  
    //field table nya dimasukin  
    public $userId;
    public $name;
    public $contact;
    public $birthYear;
    public $createdDate;
  
    // bikin construct untuk konesi database
    public function __construct($db){
        $this->conn = $db;
    
    }
    

    // Get categories
    public function read() {
        // query
        $query = "SELECT * from master_user_tester WHERE userId = ?";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->userId);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }

      public function readAll() {
        // query
        $query = "SELECT * from master_user_tester";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }

      public function getUser($id) {
        // query
        $query = "SELECT * from master_user_tester WHERE userId = ?";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $id);
        // Execute 
        $stmt->execute();
  
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
  
        // set properties
        $this->userId = $row['userId'];
        $this->name = $row['name'];
        $this->contact = $row['contact'];
        $this->birthYear = $row['birthYear'];
        $this->createdDate = $row['createdDate'];
      }
  
      // login function
    public function create(){
      // Create query
      $query = "INSERT into master_user_tester VALUES (:userId, :name, :contact, :birthYear, :createdDate)";
        //Prepare statement
        $stmt = $this->conn->prepare($query);

        $this->userId = htmlspecialchars(strip_tags($this->userId));
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->contact = htmlspecialchars(strip_tags($this->contact));
        $this->birthYear = htmlspecialchars(strip_tags($this->birthYear));
        $this->createdDate = htmlspecialchars(strip_tags($this->createdDate));

        $stmt->bindParam(':userId', $this->userId);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':contact', $this->contact);
        $stmt->bindParam(':birthYear', $this->birthYear);
        $stmt->bindParam(':createdDate', $this->createdDate);

        // Execute query
        if($stmt->execute()) {
            return true;
          }
          printf("Error: $s.\n", $stmt->error);
          return false;
      }
    }
