<?php
class KelompokTani{
  
    // tempat setting koneksi dan nama tabel 
    private $conn;
    private $table = "master_kel_tani";
  
    //field table nya dimasukin  
    public $ID_Kelompok_Tani;
    public $Nama_Kelompok_Tani;
    public $Kontak_Person;
    public $Nomor_Telpon;
    public $Foto1;
    public $Alamat_Sekretariat;
    public $Kecamatan;
    public $Kabupaten;
    public $Provinsi;
    public $ID_User;
    public $nama_admin;
    public $Email;
    public $Tgl_Terbentuk;
  
    // bikin construct untuk konesi database
    public function __construct($db){
        $this->conn = $db;
    
    }
    
    public function read() {
        // query
        $query = "SELECT k.ID_Kelompok_Tani, k.Nama_Kelompok_Tani, k.Alamat_Sekretariat, k.Kabupaten, k.Kecamatan, k.Provinsi, k.Foto1, k.Kontak_Person, k.Nomor_Telpon, k.ID_User, u.nama AS nama_admin, k.Tgl_Terbentuk, k.Email FROM master_kel_tani AS k LEFT JOIN master_detail_user AS u ON k.ID_User = u.ID_User";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }

    public function searchAll() {
        // query
        $query = "SELECT k.ID_Kelompok_Tani, k.Nama_Kelompok_Tani, k.Alamat_Sekretariat, k.Kabupaten, k.Kecamatan, k.Provinsi, k.Foto1, k.Kontak_Person, k.Nomor_Telpon, k.ID_User, u.nama AS nama_admin, k.Tgl_Terbentuk, k.Email FROM master_kel_tani AS k, master_detail_user AS u WHERE k.ID_User = u.ID_User AND k.Nama_Kelompok_Tani LIKE ?";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        $this->Nama_Kelompok_Tani = "%".$this->Nama_Kelompok_Tani."%";
        // Bind ID
        $stmt->bindParam(1, $this->Nama_Kelompok_Tani);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }
  
      // cari spesifik instansi
    public function search(){
      // Create query
      $query = "SELECT k.ID_Kelompok_Tani, k.Nama_Kelompok_Tani, k.Alamat_Sekretariat, k.Kabupaten, k.Kecamatan, k.Provinsi, k.Foto1, k.Kontak_Person, k.Nomor_Telpon, k.ID_User, u.nama AS nama_admin, k.Tgl_Terbentuk, k.Email FROM master_kel_tani AS k LEFT JOIN master_detail_user AS u ON k.ID_User = u.ID_User where k.ID_Kelompok_Tani = ? ";
  
        //Prepare statement
        $stmt = $this->conn->prepare($query);
  
        // Bind ID
        $stmt->bindParam(1, $this->ID_Kelompok_Tani);
  
        // Execute query
        $stmt->execute();
  
        return $stmt;
    }

    public function searchID($id){
      // Create query
      $query = "SELECT k.ID_Kelompok_Tani, k.Nama_Kelompok_Tani, k.Alamat_Sekretariat, k.Kabupaten, k.Kecamatan, k.Provinsi, k.Foto1, k.Kontak_Person, k.Nomor_Telpon, k.ID_User, u.nama AS nama_admin, k.Tgl_Terbentuk, k.Email FROM master_kel_tani AS k LEFT JOIN master_detail_user AS u ON k.ID_User = u.ID_User where k.ID_Kelompok_Tani = '$id' ";
        $id = htmlspecialchars(strip_tags($id));
        //Prepare statement
        $stmt = $this->conn->prepare($query);
    
        // Execute query
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
  
        // set properties
        $this->ID_Kelompok_Tani = $row['ID_Kelompok_Tani'];
        $this->Nama_Kelompok_Tani = $row['Nama_Kelompok_Tani'];
        $this->Kontak_Person = $row['Kontak_Person'];
        $this->Nomor_Telpon = $row['Nomor_Telpon'];
        $this->Foto1 = $row['Foto1'];
        $this->Alamat_Sekretariat = $row['Alamat_Sekretariat'];
        $this->Kecamatan = $row['Kecamatan'];
        $this->Kabupaten = $row['Kabupaten'];
        $this->Provinsi = $row['Provinsi'];
        $this->ID_User = $row['ID_User'];
        $this->nama_admin = $row['nama_admin'];
        $this->Tgl_Terbentuk = $row['Tgl_Terbentuk'];
        $this->Email = $row['Email'];
    }

    public function searchIdUser($id){
      // Create query
      $query = "SELECT k.ID_Kelompok_Tani, k.Nama_Kelompok_Tani, k.Alamat_Sekretariat, k.Kabupaten, k.Kecamatan, k.Provinsi, k.Foto1, k.Kontak_Person, k.Nomor_Telpon, k.ID_User, u.nama AS nama_admin, k.Tgl_Terbentuk, k.Email FROM master_kel_tani AS k LEFT JOIN master_detail_user AS u ON k.ID_User = u.ID_User where k.ID_User = '$id' ";
        $id = htmlspecialchars(strip_tags($id));
        //Prepare statement
        $stmt = $this->conn->prepare($query);
    
        // Execute query
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
  
        // set properties
        $this->ID_Kelompok_Tani = $row['ID_Kelompok_Tani'];
        $this->Nama_Kelompok_Tani = $row['Nama_Kelompok_Tani'];
        $this->Kontak_Person = $row['Kontak_Person'];
        $this->Nomor_Telpon = $row['Nomor_Telpon'];
        $this->Foto1 = $row['Foto1'];
        $this->Alamat_Sekretariat = $row['Alamat_Sekretariat'];
        $this->Kecamatan = $row['Kecamatan'];
        $this->Kabupaten = $row['Kabupaten'];
        $this->Provinsi = $row['Provinsi'];
        $this->ID_User = $row['ID_User'];
        $this->nama_admin = $row['nama_admin'];
        $this->Tgl_Terbentuk = $row['Tgl_Terbentuk'];
        $this->Email = $row['Email'];
    }
  
  
    public function create() {
      // Create Query
      $query = 'insert into '.$this->table.' 
      set ID_Kelompok_Tani = :ID_Kelompok_Tani, 
      Nama_Kelompok_Tani = :Nama_Kelompok_Tani, 
      Email = :Email, Kontak_Person = :Kontak_Person, Foto1 = :Foto1';
  
    // Prepare Statement
    $stmt = $this->conn->prepare($query);

    $this->ID_Kelompok_Tani = htmlspecialchars(strip_tags($this->ID_Kelompok_Tani));
    $this->Nama_Kelompok_Tani = htmlspecialchars(strip_tags($this->Nama_Kelompok_Tani));
    $this->Email = htmlspecialchars(strip_tags($this->Email));
    $this->Kontak_Person = htmlspecialchars(strip_tags($this->Kontak_Person));
    $this->Foto1 = htmlspecialchars(strip_tags($this->Foto1));

    // Bind data
    $stmt-> bindParam(':ID_Kelompok_Tani', $this->ID_Kelompok_Tani);
    $stmt-> bindParam(':Nama_Kelompok_Tani', $this->Nama_Kelompok_Tani);
    $stmt-> bindParam(':Email', $this->Email);
    $stmt-> bindParam(':Kontak_Person', $this->Kontak_Person);
    $stmt-> bindParam(':Foto1', $this->Foto1);
  
    // Execute query
    if($stmt->execute()) {
      return true;
    }
    printf("Error: $s.\n", $stmt->error);
    return false;
    }

    public function regis() {
      // Create Query
      $query1 = 'INSERT into master_kel_tani (ID_Kelompok_Tani, Nama_Kelompok_Tani, Alamat_Sekretariat, Provinsi, Kabupaten, Kecamatan, Desa_Kelurahan, ID_User, Email, Nomor_Telpon)
      VALUES (:ID_Kelompok_Tani, :Nama_Kelompok_Tani, :Alamat_Sekretariat, :Provinsi, :Kabupaten, :Kecamatan, :Desa_Kelurahan, :ID_User, :Email, :Nomor_Telpon)';
      
    // Prepare Statement
    $stmt = $this->conn->prepare($query1);

    $this->ID_Kelompok_Tani = htmlspecialchars(strip_tags($this->ID_Kelompok_Tani));
    $this->Nama_Kelompok_Tani = htmlspecialchars(strip_tags($this->Nama_Kelompok_Tani));
    $this->Alamat_Sekretariat = htmlspecialchars(strip_tags($this->Alamat_Sekretariat));
    $this->Kabupaten = htmlspecialchars(strip_tags($this->Kabupaten));
    $this->Kecamatan = htmlspecialchars(strip_tags($this->Kecamatan));
    $this->Provinsi = htmlspecialchars(strip_tags($this->Provinsi));
    $this->Desa_Kelurahan = htmlspecialchars(strip_tags($this->Desa_Kelurahan));    
    $this->Nomor_Telpon = htmlspecialchars(strip_tags($this->Nomor_Telpon));
    $this->Email = htmlspecialchars(strip_tags($this->Email));
    // $this->Kontak_Person = htmlspecialchars(strip_tags($this->Kontak_Person));
    $this->ID_User = htmlspecialchars(strip_tags($this->ID_User));
    // $this->nama = htmlspecialchars(strip_tags($this->nama));
    // $this->nomor_telpon = htmlspecialchars(strip_tags($this->nomor_telpon));
    // $this->Email = htmlspecialchars(strip_tags($this->Email));
    // $this->alamat = htmlspecialchars(strip_tags($this->alamat));
    // $this->kabupaten = htmlspecialchars(strip_tags($this->kabupaten));
    // $this->kecamatan = htmlspecialchars(strip_tags($this->kecamatan));
    // $this->provinsi = htmlspecialchars(strip_tags($this->provinsi));
    // $this->kelurahan_desa = htmlspecialchars(strip_tags($this->kelurahan_desa));
    // $this->Forget_Pass = htmlspecialchars(strip_tags($this->Forget_Pass));
    // $this->Password = htmlspecialchars(strip_tags($this->Password));

    // Bind data
    $stmt-> bindParam(':ID_Kelompok_Tani', $this->ID_Kelompok_Tani);
    $stmt-> bindParam(':Nama_Kelompok_Tani', $this->Nama_Kelompok_Tani);
    $stmt-> bindParam(':Alamat_Sekretariat', $this->Alamat_Sekretariat);
    $stmt-> bindParam(':Kabupaten', $this->Kabupaten);
    $stmt-> bindParam(':Kecamatan', $this->Kecamatan);
    $stmt-> bindParam(':Provinsi', $this->Provinsi);
    $stmt-> bindParam(':Desa_Kelurahan', $this->Desa_Kelurahan);
    $stmt-> bindParam(':Nomor_Telpon', $this->Nomor_Telpon);
    $stmt-> bindParam(':Email', $this->Email);
    // $stmt1-> bindParam(':Kontak_Person', $this->Kontak_Person);
    $stmt-> bindParam(':ID_User', $this->ID_User);
    
    // $stmt2-> bindParam(':ID_User', $this->ID_User);
    // $stmt2-> bindParam(':nama', $this->nama);
    // $stmt2-> bindParam(':nomor_telpon', $this->nomor_telpon);
    // $stmt2-> bindParam(':Email', $this->Email);
    // $stmt2-> bindParam(':alamat', $this->alamat);
    // $stmt2-> bindParam(':kabupaten', $this->kabupaten);
    // $stmt2-> bindParam(':kecamatan', $this->kecamatan);
    // $stmt2-> bindParam(':provinsi', $this->provinsi);
    // $stmt2-> bindParam(':kelurahan_desa', $this->kelurahan_desa);
    // $stmt2-> bindParam(':Forget_Pass', $this->Forget_Pass);
    
    // $stmt3-> bindParam(':ID_User', $this->ID_User);
    // $stmt3-> bindParam(':Password', $this->Password);

  
    // Execute query
    if($stmt->execute()) {
      return true;
    }
    printf("Error: $s.\n", $stmt->error);
    return false;
  }

  public function updateStore() {
    // Create Query
    $query = "UPDATE master_kel_tani
    SET
    Nama_Kelompok_Tani = CASE WHEN COALESCE(:Nama_Kelompok_Tani, '') = '' THEN Nama_Kelompok_Tani ELSE :Nama_Kelompok_Tani END,
    Alamat_Sekretariat = CASE WHEN COALESCE(:Alamat_Sekretariat, '') = '' THEN Alamat_Sekretariat ELSE :Alamat_Sekretariat END,
    Nomor_Telpon = CASE WHEN COALESCE(:Nomor_Telpon, '') = '' THEN Nomor_Telpon ELSE :Nomor_Telpon END,
    Email = CASE WHEN COALESCE(:Email, '') = '' THEN Email ELSE :Email END
      WHERE
      ID_Kelompok_Tani = :ID_Kelompok_Tani";

  // Prepare Statement
  $stmt = $this->conn->prepare($query);

  // Clean data
  $this->ID_Kelompok_Tani = htmlspecialchars(strip_tags($this->ID_Kelompok_Tani));
  $this->Nama_Kelompok_Tani = htmlspecialchars(strip_tags($this->Nama_Kelompok_Tani));
  $this->Alamat_Sekretariat = htmlspecialchars(strip_tags($this->Alamat_Sekretariat));
  $this->Email = htmlspecialchars(strip_tags($this->Email));
  $this->Nomor_Telpon = htmlspecialchars(strip_tags($this->Nomor_Telpon));
  

  // Bind data
  $stmt-> bindParam(':ID_Kelompok_Tani', $this->ID_Kelompok_Tani);
  $stmt-> bindParam(':Nama_Kelompok_Tani', $this->Nama_Kelompok_Tani);
  $stmt-> bindParam(':Alamat_Sekretariat', $this->Alamat_Sekretariat);
  $stmt-> bindParam(':Email', $this->Email);
  $stmt-> bindParam(':Nomor_Telpon', $this->Nomor_Telpon);

  // Execute query
  if($stmt->execute()) {
    return true;
  }

  // Print error if something goes wrong
  printf("Error: $s.\n", $stmt->error);

  return false;
  }
  





    // Update Category
    public function update() {
      // Create Query
      $query = 'update ' .
        $this->table . '
      SET
      Nama_Kelompok_Tani = :Nama_Kelompok_Tani,
      Email = :Email, Kontak_Person = :Kontak_Person,
      Foto1 = :Foto1
        WHERE
        ID_Kelompok_Tani = :ID_Kelompok_Tani';
  
    // Prepare Statement
    $stmt = $this->conn->prepare($query);
  
    // Clean data
    $this->ID_Kelompok_Tani = htmlspecialchars(strip_tags($this->ID_Kelompok_Tani));
    $this->Nama_Kelompok_Tani = htmlspecialchars(strip_tags($this->Nama_Kelompok_Tani));
    $this->Email = htmlspecialchars(strip_tags($this->Email));
    $this->Kontak_Person = htmlspecialchars(strip_tags($this->Kontak_Person));
    $this->Foto1 = htmlspecialchars(strip_tags($this->Foto1));
    

    // Bind data
    $stmt-> bindParam(':ID_Kelompok_Tani', $this->ID_Kelompok_Tani);
    $stmt-> bindParam(':Nama_Kelompok_Tani', $this->Nama_Kelompok_Tani);
    $stmt-> bindParam(':Email', $this->Email);
    $stmt-> bindParam(':Kontak_Person', $this->Kontak_Person);
    $stmt-> bindParam(':Foto1', $this->Foto1);
  
    // Execute query
    if($stmt->execute()) {
      return true;
    }
  
    // Print error if something goes wrong
    printf("Error: $s.\n", $stmt->error);
  
    return false;
    }
  


    // Delete 
    public function delete() {
    
      $query = 'DELETE FROM ' . $this->table . ' WHERE ID_Kelompok_Tani = :ID_Kelompok_Tani';
      $stmt = $this->conn->prepare($query);
  
      // clean data
      $this->ID_Kelompok_Tani = htmlspecialchars(strip_tags($this->ID_Kelompok_Tani));
  
      // Bind Data
      $stmt-> bindParam(':ID_Kelompok_Tani', $this->ID_Kelompok_Tani);
  
      // Execute query
      if($stmt->execute()) {
        return true;
      }
      printf("Error: $s.\n", $stmt->error);
  
      return false;
      }
    }