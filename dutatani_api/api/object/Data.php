<?php
class Data{
  
    // tempat setting koneksi dan nama tabel 
    private $conn;
  
    //field table nya dimasukin  
    public $id;
    public $data;
  
    // bikin construct untuk konesi database
    public function __construct($db){
        $this->conn = $db;
    }
    

    // Get categories
    public function satuan() {
        // query
        $query = "SELECT * FROM master_satuan_saprotan";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }

      public function desa($kec) {
        // query
        $query = "SELECT DISTINCT Nama_Desa FROM kelurahan_desa WHERE Nama_Kecamatan LIKE '$kec'";
        $kec = "%".$kec."%";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }

      public function kecamatan($kab) {
        // query
        $query = "SELECT Nama_Kecamatan FROM kecamatan WHERE Nama_Kabupaten LIKE '$kab'";
        $kab = "%".$kab."%";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }

      public function kabupaten($prov) {
        // query
        $query = "SELECT Nama_Kabupaten FROM kabupaten WHERE Nama_Provinsi LIKE '$prov'";
        $prov = "%".$prov."%";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }

      public function provinsi() {
        // query
        $query = "SELECT * FROM provinsi";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }

      public function kategori() {
        // query
        $query = "SELECT * FROM master_kategori_alatbahan";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }

    
    }