<?php
class TestActivity{
  
    // tempat setting koneksi dan nama tabel 
    private $conn;
  
    //field table nya dimasukin  
    public $session;
    public $user;
    public $activities;
    public $activityTime;
  
    // bikin construct untuk konesi database
    public function __construct($db){
        $this->conn = $db;
    
    }
    

    // Get categories
    public function read() {
        // query
        $query = "SELECT user, activities, activityTime from log_testing_activity WHERE session = ?";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->session);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }

      public function readTaskUser() {
        // query
        $query = "SELECT session, activities, activityTime from log_testing_activity WHERE user = ?";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->user);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }
  
      // login function
    public function create(){
      // Create query
      $query = "INSERT into log_testing_activity(session, user, activities, activityTime) VALUES (:session, :user, :activities, :activityTime)";
        //Prepare statement
        $stmt = $this->conn->prepare($query);

        $this->session = htmlspecialchars(strip_tags($this->session));
        $this->user = htmlspecialchars(strip_tags($this->user));
        $this->activities = htmlspecialchars(strip_tags($this->activities));
        $this->activityTime = htmlspecialchars(strip_tags($this->activityTime));

        $stmt->bindParam(':session', $this->session);
        $stmt->bindParam(':user', $this->user);
        $stmt->bindParam(':activities', $this->activities);
        $stmt->bindParam(':activityTime', $this->activityTime);

        // Execute query
        if($stmt->execute()) {
            return true;
          }
          printf("Error: $s.\n", $stmt->error);
          return false;
      }
    }
