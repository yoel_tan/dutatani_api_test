<?php
class User{
  
    // tempat setting koneksi dan nama tabel 
    private $conn;
    private $table = "master_detail_user";
  
    //field table nya dimasukin  
    public $ID_User;
    public $nama;
    public $Email;
    public $nomor_telpon;
    public $Password;
    public $ID_Kelompok_Tani;
    public $jawaban;
  
    // bikin construct untuk konesi database
    public function __construct($db){
        $this->conn = $db;
    
    }
    

    // Get categories
    public function read() {
        // query
        $query = "SELECT d.nama, d.Email, k.ID_Kelompok_Tani from master_detail_user AS d, master_user AS u, master_kel_tani AS k WHERE d.ID_User = u.ID_User AND d.ID_User = k.ID_User AND u.ID_User = ?";
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->ID_User);
        // Execute 
        $stmt->execute();
  
        return $stmt;
      }
  
      // login function
    public function login(){
      // Create query
      $query = "SELECT k.ID_User FROM master_user AS u, master_kel_tani AS k WHERE k.ID_User = u.ID_User AND k.ID_User = :ID_User AND u.password = :pass";
        //Prepare statement
        $stmt = $this->conn->prepare($query);

        $this->ID_User = htmlspecialchars(strip_tags($this->ID_User));
        $this->pass = htmlspecialchars(strip_tags($this->pass));

        $stmt-> bindParam(':ID_User', $this->ID_User);
        $stmt-> bindParam(':pass', $this->pass);
  
        // Execute query
        $stmt->execute();
  
        return $stmt;
      }

      public function checkUser(){
        // Create query
        $query = "SELECT u.jawaban, u.Password FROM master_user AS u, master_kel_tani AS k WHERE k.ID_User = u.ID_User AND k.ID_User = :ID_User";
          //Prepare statement
          $stmt = $this->conn->prepare($query);
  
          $this->ID_User = htmlspecialchars(strip_tags($this->ID_User));
  
          $stmt-> bindParam(':ID_User', $this->ID_User);
    
          $stmt->execute();
    
          return $stmt;
      }

      public function forgotPass(){
        // Create query
        $query = "UPDATE master_user
        SET
        Password = :pass
          WHERE ID_User = :ID_User";
          //Prepare statement
          $stmt = $this->conn->prepare($query);
  
          $this->Password = htmlspecialchars(strip_tags($this->pass));
          $this->ID_User = htmlspecialchars(strip_tags($this->ID_User));

          $stmt->bindParam(':pass', $this->Password);
          $stmt-> bindParam(':ID_User', $this->ID_User);
    
          // Execute query
          $stmt->execute();
    
          return $stmt;
      }

      public function regUser() {
        $queryMaster = "INSERT into master_user
          VALUES (:ID_User, :Password, :jawaban, 123456, 1)";

        $queryDetail = 'INSERT into master_detail_user(ID_User, nama, nomor_telpon, Email)
          VALUES (:ID_User, :nama, :nomor_telpon, :Email)';

        $stmtMaster = $this->conn->prepare($queryMaster);
        $stmtDetail = $this->conn->prepare($queryDetail);

        $this->ID_User = htmlspecialchars(strip_tags($this->ID_User));
        $this->nama = htmlspecialchars(strip_tags($this->nama));
        $this->nomor_telpon = htmlspecialchars(strip_tags($this->nomor_telpon));
        $this->Email = htmlspecialchars(strip_tags($this->Email));
        $this->Password = htmlspecialchars(strip_tags($this->Password));
        $this->jawaban = htmlspecialchars(strip_tags($this->jawaban));

        $stmtMaster->bindParam(':ID_User', $this->ID_User);
        $stmtMaster->bindParam(':Password', $this->Password);
        $stmtMaster->bindParam(':jawaban', $this->jawaban);
        $stmtDetail->bindParam(':ID_User', $this->ID_User);
        $stmtDetail->bindParam(':nama', $this->nama);
        $stmtDetail->bindParam(':nomor_telpon', $this->nomor_telpon);
        $stmtDetail->bindParam(':Email', $this->Email);

        if($stmtMaster->execute()) {
          if($stmtDetail->execute()) {
            return true;
          }
          else {
            printf("Error: $s.\n", $stmtDetail->error);
            return false; 
          }
        }
        // Print error if something goes wrong
        printf("Error: $s.\n", $stmtMaster->error);
        return false;

      } 

      public function updateUser() {
        $query = "UPDATE master_detail_user
          SET
          nama = CASE WHEN COALESCE(:nama, '') = '' THEN nama ELSE :nama END,
          nomor_telpon = CASE WHEN COALESCE(:nomor_telpon, '') = '' THEN nomor_telpon ELSE :nomor_telpon END,
          Email = CASE WHEN COALESCE(:Email, '') = '' THEN Email ELSE :Email END
            WHERE
            ID_User = :ID_User";

        $stmt = $this->conn->prepare($query);

        $this->ID_User = htmlspecialchars(strip_tags($this->ID_User));
        $this->nama = htmlspecialchars(strip_tags($this->nama));
        $this->nomor_telpon = htmlspecialchars(strip_tags($this->nomor_telpon));
        $this->Email = htmlspecialchars(strip_tags($this->Email));

        $stmt->bindParam(':ID_User', $this->ID_User);
        $stmt->bindParam(':nama', $this->nama);
        $stmt->bindParam(':nomor_telpon', $this->nomor_telpon);
        $stmt->bindParam(':Email', $this->Email);

        if($stmt->execute()) {
          return true;
        }
      
        // Print error if something goes wrong
        printf("Error: $s.\n", $stmt->error);
        return false;

      }

    }
